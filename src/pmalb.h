/**
 * \file
 * \mainpage
 * @author Paul Maruhn
 * @author Roni Draether
 *
 * This <b>P</b>ass <b>m</b>e <b>a</b> <b>L</b>ink <b>b</b>ro
*/

#ifndef PMALB_H_
#define PMALB_H_

#include <stdbool.h>

#define APP_NAME "pmalb"
#define APP_ORG "org.pauni.pmalb"

#define KEY_HOSTNAME "hostname"
#define KEY_TCP_PORT "tcp_port"

#define KEY_URL "url"
#define KEY_FULLSCREEN "fullscreen"

void p_log(char *file, const char *func, char *msg, ...);

bool compare(const char *s1, const char *s2);

struct Params {
    int argc;
    char **argv;
};

#endif
