#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <jansson.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <ctype.h>
#include <argp.h>

#include "net.h"
#include "gui.h"
#include "ipc.h"
#include "pmalb.h"
#include "log.h"
#include "userdata.h"
#include "discovery.h"


/** Authors: Roni Dräther, Paul Maruhn
 *
 *	Main file of Pmalb. Entry point and core center of the application.
 *	This file launches the TCP and GUI threads and listenes for IPC messages.
 *	It is the first and last to start/close and can be terminated by
 *	firstly disabeling the toggle and secondly closing the GUI.
*/


static pthread_t gui_thread; // gui thread
static pthread_t tcp_srv_thread; // server thread
static pthread_t udp_srv_thread; // server thread
static pthread_t dscvry_thread; // server thread


// The options we understand
static struct argp_option options[] = {
	{"verbose", 'v', 0, 0, "Produce verbose output"},
	{"foreground", 'f', 0, 0, "don't daemonize"},
	{"stop", 's', 0, 0, "stop pmalb daemon"},
	{0}
};



// our handy arguments struct
struct arguments {
	char* args[3];
	// start, but don't detach
	int foreground;
	int verbose;
	// stop the daemon
	int stop;
};


// set the values
static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	/* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
	struct arguments *arguments = state->input;

	switch (key) {
	case 'v':
		arguments->verbose = 1;
		break;
	case 'f':
		arguments->foreground = 1;
		break;
	case 's':
		arguments->stop = 1;
		break;

	case ARGP_KEY_ARG:
		if (state->arg_num > 0 )
			/* Too many arguments. */
			argp_usage(state);

		arguments->args[state->arg_num] = arg;
		break;
	default:
		return ARGP_ERR_UNKNOWN;
	}

	return 0;
}


// parser
static struct argp argp = {options, parse_opt, 0, 0};


// cleanup things when on unexpected situations
void unprepared_pmalb_stop() {
	close_ipc_socket();
}

// register signal handler for ctrl+c


// checks whether GUI is running. If not: create thread and launch GUI
void start_gui_thread() {
    if (gui_is_running()) {
		log_debug( "GUI is already running.");
		return;
    }

    log_debug( "starting GUI:");
    pthread_create(&gui_thread, NULL, init_gtk_application, NULL);
}


// checks whether tcp-server is running, starts tcp-server on false on seperate thread
void start_server_thread() {
    if (server_is_running()) {
		log_debug( "Tcp-server already running.");
		return;
    }

	// i think it would be better if the "daemon" and server stay it the main process
    pthread_create(&tcp_srv_thread, NULL, (void *) start_server, NULL);
    pthread_create(&udp_srv_thread, NULL, (void *) start_udp_server, NULL);
	sleep(1);
    pthread_create(&dscvry_thread, NULL, (void *) start_discovery_service, NULL);

	// notify_is_connected();
}

// checks whether another instance is running. If so, request's gui to show
void check_whether_single_instance() {
    if (!is_single_instance()) {
		log_debug( "Pmalb is alread running. Closing and request to open GUI");
		send_ipc_message(IPC_SHOW_GUI);
		exit(0);
    }
    log_debug("Is single instance! Proceed.");

}
// checks whether pmalb is placed in the correct directory
void check_installation_path() {
    char choice;

    if (access("/opt/pmalb", F_OK) == 0) {
		log_debug( "Pmalb folder found in /opt.");
		return;
    }

    printf("It seems that Pmalb is not installed correctly! It has to be copied to /opt/pmalb.\n");
    printf("Do you want to fix it automatically? (y/N):");
    scanf("%c", &choice);

    if (choice == 'y' || choice == 'Y') {
		printf("cool\n"); //TODO: implement automatic installation
	}
    else {
		printf("Pmalb might not work properly. Continue? (y/N):");
		scanf("%c", &choice);

		if (choice != 'y' && choice != 'Y')
	    	exit(0);
    }
}


// the main loop listens on unix domain socket to start/close gui/tcp-server
void main_loop() {
    int buf_size = 1024;
    IpcMessage message; // to write ipc_stream in

    log_debug("entering main loop");
    while (gui_is_running() || server_is_running()) {
    	receive_ipc_message(&message, buf_size);

		log_debug("receiving ipc signal: %s", ipc_message_text(message));

		switch (message) {
			case IPC_SHOW_GUI:
				start_gui_thread();
				break;

			case IPC_START_SERVER:
				start_server_thread();
				break;

			case IPC_STOP_SERVER:
				close_server();
				pthread_cancel(tcp_srv_thread);
				pthread_cancel(udp_srv_thread);
				break;

			default:
				log_warn("an ipc message was receive that is not implemented");
		}
    }

    log_debug( "exit main loop");
    close_ipc_socket(); // close unix domain socket
}

void testing() {
	json_t *array = json_array();
	json_array_append(array, json_string("apple"));
	json_array_append(array, json_string("banana"));
	json_array_append(array, json_string("lemon"));

	for (int i=0; i<json_array_size(array); i++) {
		log_debug("%s", json_string_value(json_array_get(array, i)));
	}
}

// entry point of the application
int main(int argc, char *argv[]) {
	// this struct holds the status of the discovery service
	// and a list of found devices

	struct arguments arguments;
	// set our defaults
	arguments.verbose = 0;
	arguments.foreground = 0;
	arguments.stop = 0;
	// Our argp parser.
	argp_parse(&argp, argc, argv, 0, NULL, &arguments);

	// force most verbose loglevel on debug builds
#ifdef BUILD_DEBUG
	log_set_level(0);
#else
	log_set_level(3);
	if (arguments.verbose) {
		log_set_level(0);
	}
#endif


	init_devicemanager();



    printf("\n********************\n*  Started Pmalb!  *\n********************\n\n\n");

    check_whether_single_instance();	// check for other instances
    //check_installation_path();	// check for correct installation path
    start_gui_thread();			// show gui with QR-code for pairing
    start_server_thread();		// start the tcp-server to receive links
    usleep(100000);			// wait for gui/server to be created
    main_loop();			// loops until gui AND server are closed

    printf("\n********************\n*  Exit    Pmalb!  *\n********************\n\n\n");

	return 0;
}

bool compare(const char *s1, const char *s2) {
	if (s1 == NULL || s2 == NULL) {
		log_warn("One of the compare strings are NULL!\ns1: %s\ns2: %s", s1, s2);
		return false;
	}

  return strcmp(s1, s2) == 0;

}

// Pmalb log function to print into terminal
// Usage: p_log( <formatted string>, <params...>);
int log_count = 0;
void p_log(char *file, const char *func, char *msg, ...){
    va_list arg;

    printf("[%d: %s: %s]: ", log_count, file, func);
    va_start(arg, msg);
    vfprintf(stdout, msg, arg);
    va_end(arg);
    printf("\n");
    log_count++;
}
