#include <assert.h>
#include <jansson.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sodium.h>
#include <netinet/in.h>
#include <unistd.h>

#include "gui.h"
#include "log.h"
#include "pmalb.h"
#include "protocol.h"
#include "userdata.h"

// do not use outside of this file
#define FP_LINKED_PHONES "/tmp/linked_phones.json"  // file path of connected phone (stored as json)
#define PREFS_FORMAT "{s:s, s:s, s:O}"



char *load(const char *filename);
bool save(const char *filename, char *string, int length);

uint32_t presecret;



/// save all discovered devices here
struct discovered_device {
	unsigned char* public_key[PUBKEYLEN];
	struct in6_addr last_ip_addr;
};



struct device_manager {
	unsigned char secret[SECKEYLEN];
	unsigned char public[PUBKEYLEN];
	struct paired_device* devices;
	struct discovered_devices *discovered_devices;
} device_mngr;


unsigned char *get_own_public_key(void) {
	return (unsigned char*) device_mngr.public;
}


unsigned char *get_own_public_signkey(void) {
	return (unsigned char*) device_mngr.public;
}


unsigned char *get_own_public_cryptokey(void) {
	return (unsigned char*) device_mngr.public + crypto_sign_PUBLICKEYBYTES;
}



unsigned char *get_own_secret_key(void) {
	return (unsigned char*) device_mngr.secret;
}


unsigned char *get_own_secret_signkey(void) {
	return (unsigned char*) device_mngr.secret;
}


unsigned char *get_own_secret_cryptokey(void) {
	return (unsigned char*) device_mngr.secret + crypto_sign_SECRETKEYBYTES;
}




int crc32(unsigned char* data, int length ) {
	#define POLYNOMIAL 0x04C11DB7

	int32_t crcsum = 0xffffffff;
	int32_t table[256];
	int32_t r;

	for (size_t i = 0; i < 256; i++) {
		r = i << (8 * sizeof(crcsum) - 8);

		for (size_t j = 8; j > 0 ; i--) {
			if (r & 0x80000000) {
				r = (r << 1) ^ POLYNOMIAL;
			}
			else {
				r = (r << 1);
			}
		}

		table[i] = r;
	}


	for (size_t i = 0; i < length; i++) {
		int n = (crcsum ^ data[i]) && 0xff; // 0xff to prevent out of bound access
		crcsum = (crcsum >> 8) ^ table[n];
	}

	crcsum ^= 0xffffffff;
	return crcsum;
}


/// convert binary data to nullterminated hexstring
char *hexencode(unsigned char *bin, size_t len) {
	// log_trace("mallocing %d bytes", (int) len*2+1);

	char *out = (char*) malloc(len*2+1);
	for (size_t i = 0; i < len; i++) {
		out[i*2]   = "0123456789abcdef"[bin[i] >> 4];
		out[i*2+1] = "0123456789abcdef"[bin[i] & 0x0F];
	}
	out[len*2] = '\0';

	return out;
}


/// calling function must make sure that enough byte were allocated
size_t hexdecode(const char *hex, unsigned char **out) {
	size_t len = strlen(hex);

	log_trace("hexstring length %d", len);

	if (len % 2 != 0) {
		return 0;
	}

	// log_trace("hexdecode: out pointer: %p", out);
	// log_trace("hexdecode: out pointer pointer: %p", *out);

	*out = calloc(1, len/2);

	for (size_t i = 0; i < len; i++) {
		unsigned char val;

		if (hex[i] >= '0' && hex[i] <= '9') {
			val = hex[i] - '0';
		} else if (hex[i] >= 'A' && hex[i] <= 'F') {
			val = hex[i] - 'A' + 10;
		} else if (hex[i] >= 'a' && hex[i] <= 'f') {
			val = hex[i] - 'a' +10;
		} else {
			log_error("%c is not a valid hex value", hex[i]);
			log_error("unrecoverable error");
			exit(1);
		}

		// log_trace("convert %c to %x", hex[i], val);

		(*out)[i/2] = (*out)[i/2] | (val << (!(i % 2 )*4));
		// log_trace("decoding: charbuffer now: %x", (*out)[i/2]);
	}

	// log_trace("hexdecode: out pointer: %p", out);
	// log_trace("hexdecode: out pointer pointer: %p", *out);
	return len;
}


bool get_phone(struct Phone *phone, int index) {
    // read json string from file
    json_error_t jerror;

	json_t *jsn_phones = json_loads(PREFERENCES_FILE, 0, &jerror);
    if (jsn_phones == NULL) {
		free(jsn_phones);
		return false;
    }

    // retrieve item at 'index'
    json_t *jsn_phone = json_array_get(jsn_phones, index);
    if (jsn_phone == NULL) {
		log_fatal("Failed to get item #%d from array: %s", index, json_dumps(jsn_phones, 0));
		free(jsn_phones);
		return false;
    }

    // parse json into struct 'phone'
    phone->id = (int *) json_integer_value(json_object_get(jsn_phone, PHONE_ID));
	phone->ip = (char*) json_string_value(json_object_get(jsn_phone, PHONE_IP));
    phone->name = (char*) json_string_value(json_object_get(jsn_phone, PHONE_NAME));
    phone->port = json_integer_value(json_object_get(jsn_phone, PHONE_PORT));

	free(jsn_phones);
    return true;
}





struct paired_device *get_paired_device(unsigned char *pubkey) {
	for (struct paired_device *device = device_mngr.devices; device != NULL; device = device->next) {
		if ( memcmp(&device->public_key , pubkey, PUBKEYLEN) ) {
			log_trace("found public key");
			return device;
		}
	}

	log_trace("device is not paired");

	return NULL;
}





json_t *get_phones() {
    json_t *root  = NULL;

    // load phones from file
    // json_t *jsn_str = json_load_file(PREFERENCES_FILE, 0, NULL);
    // if (jsn_str != NULL) {
	// 	root = json_loads(jsn_str, 0, NULL);
    // }
    // free(jsn_str);
    return root;
}

bool set_selected_phones(int id) {
    size_t index;
    json_t *value;
    json_t *jsn_phones = get_phones();

    json_array_foreach(jsn_phones, index, value) {
		if (id == json_integer_value(json_object_get(value, PHONE_ID))) {
			log_debug("match at %d. Apply MTF", index);

			// Apply MTF (do insert, remove. Otherway around doesn't work for some reason)
			json_array_insert(jsn_phones, 0, value);
			json_array_remove(jsn_phones, index+1);
		}
    }

    char *jsn_str = json_dumps(jsn_phones, 0);
    log_debug("jsonstring %s", jsn_str);

    free(jsn_phones);
    bool success = (strlen(jsn_str) == save_config(FP_LINKED_PHONES, jsn_str, strlen(jsn_str)));
    // update_devices_list();
    return success;
}





int load_config(void) {
	json_error_t json_error;

	json_t* config = json_load_file(PREFERENCES_FILE, 0, &json_error);

	if (!config) {
		log_error("can't load json file: %s", json_error.text);
		return -1;
	}



	json_t* devices = json_array();

	char *hex_pk, *hex_sk;
	unsigned char *pk, *sk;

	// using a shortcut to load keys
	int err = json_unpack_ex(config, &json_error, 0, PREFS_FORMAT,
		"secret", &hex_sk,
		"public", &hex_pk,
		"devices", &devices
	);


	assert(strlen(hex_pk) != PUBKEYLEN);
	assert(strlen(hex_sk) != SECKEYLEN);


	// log_info("read public key: %s", pk);
	hexdecode(hex_pk, &pk);
	hexdecode(hex_sk, &sk);
	memcpy(&device_mngr.public, pk, PUBKEYLEN);
	memcpy(&device_mngr.secret, sk, SECKEYLEN);

	log_trace("decoded public key: %s", hexencode(pk, PUBKEYLEN));
	log_trace("decoded secret key: %s", hexencode((unsigned char*) device_mngr.secret, SECKEYLEN));

	if (err != 0) {
		log_error("unable to parse config file: %s at %s:%d:%d",
			json_error.text,
			json_error.source,
			json_error.line,
			json_error.column
		);

		return -1;
	}

	if (!json_array_size(devices)) {
		log_warn("no devices loaded");
	} else {
		log_warn("Todo: add devices");
	}



	return 0;
}



/// long function that saves all preferences and devices
json_t* prefs_to_json(void) {
	json_t* device_list = json_array();

	for (struct paired_device* d = device_mngr.devices; d != NULL; d = d->next) {
		json_error_t err;

		char* pk = hexencode(*d->public_key, PUBKEYLEN);

		log_trace("saving device...");
		log_trace("\tpublic: %s", pk);
		log_trace("\tname: %s", d->name);
		log_trace("\tlast_seen: %d", d->last_seen);


		json_t* dev = json_pack_ex(&err, 0, "{s:s, s:s, s:i}",
			"public", pk,
			"name", d->name,
			"last_seen", d->last_seen
		);

		if (dev == NULL) {
			log_error(
				"unable to serialize device: %s at %s:%d:%d",
				err.text,
				err.source,
				err.line,
				err.column
			);

			return NULL;
		}

		log_trace("add devic to save list");
		json_array_append(device_list, dev);
	}


	int k;
	json_t *v;

	json_array_foreach(device_list, k, v) {
		log_info("name: %s", json_string_value(json_object_get(v, "name")));
		// log_info("name: %s", d->name);
		// log_info("last_seen: %d", d->last_seen);
    }


	json_error_t err;
	json_t *prefs;

	log_info("save public key %s", hexencode(get_own_public_key(), PUBKEYLEN));

	prefs = json_pack_ex(&err, 0, PREFS_FORMAT,
		"public", hexencode(get_own_public_key(), PUBKEYLEN),
		"secret", hexencode(get_own_secret_key(), SECKEYLEN),
		"devices", device_list
	);

	if (prefs == NULL) {
		log_error(
			"unable to serialize preferences: %s at %s:%d:%d",
			err.text,
			err.source,
			err.line,
			err.column
		);
		return NULL;
	}

	return prefs;
}


int save_config() {
	json_t* json = prefs_to_json();
	return json_dump_file(json, PREFERENCES_FILE, JSON_INDENT(2) | JSON_SORT_KEYS);
}



void generate_crypto_keys() {
	unsigned char cryptokey_pub[crypto_box_PUBLICKEYBYTES];
	unsigned char cryptokey_sec[crypto_box_SECRETKEYBYTES];
	unsigned char signkey_pub[crypto_sign_PUBLICKEYBYTES];
	unsigned char signkey_sec[crypto_sign_SECRETKEYBYTES];

	crypto_box_keypair(
		cryptokey_pub,
		cryptokey_sec
	);

	crypto_sign_keypair(
		signkey_pub,
		signkey_sec
	);


	memcpy(device_mngr.public, signkey_pub, crypto_sign_PUBLICKEYBYTES);
	memcpy(device_mngr.public + crypto_sign_PUBLICKEYBYTES, cryptokey_pub, crypto_box_PUBLICKEYBYTES);

	memcpy(device_mngr.secret, signkey_sec, crypto_sign_SECRETKEYBYTES);
	memcpy(device_mngr.secret + crypto_sign_SECRETKEYBYTES, cryptokey_sec, crypto_box_SECRETKEYBYTES);
}

/// check crypto and device and stuff
void init_devicemanager() {
	// device_mngr.public[0] = (unsigned char*) malloc(crypto_box_PUBLICKEYBYTES);
	// device_mngr.secret[0] = (unsigned char*) malloc(crypto_box_SECRETKEYBYTES);


	if (access(PREFERENCES_FILE, F_OK) == 0) {
		log_info("loading keys and devices from %s", PREFERENCES_FILE);
		if (load_config() != 0) {
			log_fatal("can't load keys and devices");
			exit(1);
		}
	}
	else {
		log_warn("preferences file does not exists");
		log_info("generating keypair...");
		generate_crypto_keys();
		log_info("public key: %s", hexencode(device_mngr.public, PUBKEYLEN));

		save_config();
	}


	// generate a preshared secret for this session
	regenerate_pairing_secret();

	// unsigned char *key;
	// hexdecode("528e2a6eacf3a93484047307074b8cc157f115d40b99855a0207fe38cc95b621", &key);
	// // printf("%s\n", hexencode(key, crypto_box_PUBLICKEYBYTES));

	// struct paired_device phone = {
	// 	.next = NULL,
	// 	.name = "Test_device_123",
	// 	.public_key = key,
	// 	.last_seen = 15,
	// 	.last_ip_addr = in6addr_any
	// };


	// device_mngr.devices = &phone;

	return;
}


uint8_t get_pairing_secret() {
	return presecret;
}


void regenerate_pairing_secret(void) {
	presecret = randombytes_random();
}
