#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>

#include "ipc.h"
#include "log.h"
#include "pmalb.h"

#define SOCKET_NAME "/tmp/pmalb_socket"

static int server, client;
static struct sockaddr_un serv_addr, cli_addr;

char* ipc_message_text(IpcMessage message) {
	switch (message) {
	case IPC_SHOW_GUI:
		return "show_gui";
		break;

	case IPC_HIDE_GUI:
		return "hide_gui";
		break;

	case IPC_START_SERVER:
		return "start_server";
		break;

	case IPC_STOP_SERVER:
		return "stop_server";
		break;


	default:
		log_error("IMPLEMENT THIS NOW");
		exit(1);
	}
}

bool is_single_instance() {
    serv_addr.sun_family = AF_UNIX;
    strcpy(serv_addr.sun_path, SOCKET_NAME); // sockaddr_un.sun_path[108]

    server = socket(AF_UNIX, SOCK_STREAM, 0);
    if (bind(server, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_un))) {
		log_warn("Binding to %s failed: %s", serv_addr.sun_path, strerror(errno));
		return false;
    }

    log_debug("Binding successful. Started unix domain server \"%s\"", serv_addr.sun_path);
    return true;
}

void close_ipc_socket() {
    log_debug("killing ipc connections and delete file: %s", SOCKET_NAME);
    close(server);
    close(client);
    unlink(SOCKET_NAME);
}

void receive_ipc_message(IpcMessage *message, int buf_size) {
	log_trace("receiving ipc message");

    if (listen(server, 5)) {
		log_error("listen: %s", strerror(errno));
    };

    socklen_t cli_addr_size = sizeof(struct sockaddr_un);
    client = accept(server,  (struct sockaddr *) &cli_addr, &cli_addr_size);
    if (client == -1) {
		log_error("error while accepting: %s", strerror(errno));
    }

	log_trace("reading form ipc socket");
    read(client, message, sizeof(IpcMessage));
	log_debug("received: %#02x", *message);
}

void send_ipc_message(IpcMessage message) {
    int s;

    s = socket(AF_UNIX, SOCK_STREAM, 0);
    if (s < 0) {
		log_error("opening stream socket failed: %s", strerror(errno));
    }

    if (connect(s, (struct sockaddr *) &serv_addr, sizeof(struct sockaddr_un))) {
		log_fatal("connecting stream socket: %s", strerror(errno));
		exit(1);
    }

    log_debug("send ipc message: %s", ipc_message_text(message));
    write(s, &message, sizeof(IpcMessage));
	log_trace("ipc message sent");
}
