/** \file */

#ifndef NET_H_
#define NET_H_

#include <stdint.h>
#include <netinet/in.h>
#include <stdbool.h>

#define HOSTNAME_SIZE 255
#define PMALB_PORT 4422
#define MULICAST_GROUP "ff02::dead:d1cc:fab"

#define CONNECTION_QUEUE_SIZE 4096
#define UDP_BUFFER_SIZE 65507

#define PMALB_UDP_TYPE_DISCOVERY 0
#define PMALB_UDP_TYPE_PARING_REQEUST 1

#define PMALB_PROTO_VERSION 0

#define PMALB_ENCRYPTION_TYPE_NONE 0
#define PMALB_ENCRYPTION_TYPE_SYM 1
#define PMALB_ENCRYPTION_TYPE_ASYM 2
#define PMALB_ENCRYPTION_TYPE_BLOCKCHAIN 3

typedef uint16_t proto_type_t;



/**
 * simply set bit like pmalb_proto_flags.broadcasted = 1
 * beware! since we are using only one bit, values simply overflow
 * 2 => 0, 3 => 1 [...]
 */
typedef struct {
	unsigned int broadcasted : 1;
	unsigned int reserved : 7;
} pmalb_proto_flags_t;

/**
 * The pmalb_proto_header contains all information about the payload
 */
typedef struct {
	uint64_t payload_size;
	pmalb_proto_flags_t flags;
	uint8_t version;
	proto_type_t message_type;
} pmalb_proto_header_t;


/**
 * pmalb_proto_payload holds the payload in `buffer` and
 * the size in `size`.
 */
typedef struct {
	uint64_t size;
	uint8_t* buffer;
} pmalb_proto_payload_t;



typedef struct {
	unsigned int encryption_type: 2;
	bool multicasted: 1;
	unsigned int reserved: 5;
} pmalb_udp_packet_flags_t;


/**
 * data structure for the udp header
 */
typedef struct {
	uint8_t version;
    pmalb_udp_packet_flags_t flags;
	size_t len;
	uint16_t packet_type;
	struct sockaddr_in6* remote;
	unsigned char *payload;
} pmalb_udp_packet_t;





#include "protocol.h"



/// check if the server is runnig
int server_is_running();


/// Todo
int deserialize_device();


/// compare two ipv6 addresses (not tested)
int compare_ipv6_addr(struct in6_addr* a, struct in6_addr* b);


/// Todo
void start_server();



void *start_udp_server(void);


/// Todo
void close_server(void);


/// Todo
int pass_to_phone(const struct Phone target, char *request_type, const char *msg, const int len);


/// check if the passed address is a ipv6 multicast address
bool is_multicast_address(struct in6_addr addr);



void prepare_udp_socket(int *s);


int *get_udp_socket();

#endif
