/** \file */

#ifndef DISCOVERY_H_
#define DISCOVERY_H_

#include <stdbool.h>
#include <sodium.h>
#include <netinet/in.h>
#include "userdata.h"

#define PMALB_UDP_FLAG_ENCRYPTION 0x03
#define FLAG_MULTICASTED 0x04
#define FLAG_PUBLIC_REPLY 0x08
#define BEACON_INTERVAL 5

#define SIGNATURELEN crypto_sign_BYTES
#define DISCOVERY_BEACON_SIZE  (SIGNATURELEN + PUBKEYLEN + 4)


/**
 * Represent a discovered device by the disovery_responder
 */
struct disco_device {
	/// pointer to next device in list. NULL if end is reached
	struct disco_device* next_device;
	/// a human readable device name
	char* name;
	/// timestamp last beacon received
	time_t last_seen;
	/// an id used to identify the device
	int64_t id;
	/// publickey
	unsigned char public_key[PUBKEYLEN];
	/// Ip address of the device
	struct in6_addr remote_addr;
};


/// holds runtime data for the discovery service
struct discovery_service_status {
	bool running;
	struct disco_device* devices;
	pthread_t thread;
	int thread_exit;
};



/// A beacon struct
typedef struct {
	bool multicast;
	unsigned char *public[PUBKEYLEN];
} discovery_beacon_t;


#include "net.h"

/// start the responder
void *start_discovery_service();


/// parse bytearray to a beacon struct
void parse_discovery_beacon(pmalb_udp_packet_t *packet, discovery_beacon_t *beacon);


void recv_beacon(pmalb_udp_packet_t *beacon);



struct disco_device *get_discovered_device_by_ip(struct in6_addr addr);


#endif
