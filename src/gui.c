#include <gio/gio.h>
#include <gtk/gtk.h>
#include <jansson.h>
#include <math.h>
#include <netdb.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "net.h"
#include "protocol.h"
#include "qrcodegen.h"
#include "pmalb.h"
#include "ipc.h"
#include "log.h"
#include "gui.h"
#include "userdata.h"

#define MIN_QRCODE_SIZE 5*41
#define WHITE 255
#define BLACK 0
#define CHANNELS 3
#define PATH_GUI "./res/ui/gui.ui"
#define GAPP_NAME "pauni.pmalb"



//  VARIABLES

struct gui {
	GtkBuilder *builder;
	GdkPixbuf  *pb_qrcode; // QR-code of own hostname/port (json)
	GtkWindow  *window; // main window
	GtkWidget  *gswitch; // turn pmalb on/off
	GtkImage   *image_qrcode; // QR-code etc.
	GtkWidget  *qrcode_eventbox; // image_main container for resizing events
	GtkEntry   *entry_url; // select data to be sent
	GtkButton  *button_send; // send data to selected device
	GtkLabel *label_qrcode; // label with qrcode content
	GtkMenuButton *mbutton_devices;	      // open device list
	GtkLabel      *label_selected_device; // Displays selected device
	GtkPopover    *popover_devices;	      // popover, holds list_devices
	GtkBox	      *box_device_list;	      // list of all added devices
	GtkButton     *button_add_device;     // pair a phone with desktop client
	GtkDialog *qrcode_popup;
} guiui;


static GtkApplication *app;
static int gui_run = false;	  // whether window is open - used by main_loop
static int window_width;	  // current with of window
static int window_height;	  // current height of window




//  PROTOTYPES

// Entry point for GTK app. Prepares widgets and launces window
static void activate(GtkApplication *app , gpointer data);

// will be useful surely in the future
void update_qrcode(char *new_content, double multiplier);

// callback function for GtkSwitch. Switches the server on or off.
gboolean toggle_server(GtkSwitch *widget, gboolean state, gpointer user_data);

// callback function for anything you want. Use it for testing/debugging
gboolean debug (GtkWidget *widget, GdkRectangle *allocation, gpointer user_data);

// generates a json with hostname/port to be shown by the QR-code
char *create_pairing_info();

// input string --> qr-code (binary array) --> rgb array --> gdk_pixbuff
GdkPixbuf *generate_qrcode_pixbuf(const char *text);

// enlarges or shrinks the pixbuf by the given multiplier
GdkPixbuf *scale_pixbuf(GdkPixbuf *pixbuf, double multiplier);

// callback function to dynamically adjust the QR code's size
void qrcode_resize(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data);

// callback function of button_send. Sends url to selected device
bool send_link(GtkWidget *widget, GtkEntry *entry_url);

// callback function of combo_devices. Applies MTF of selected device
void select_device(GtkToggleButton *togglebutton, gpointer user_data);

// callback function of button_add_device
void add_device(GtkWidget *widget, gpointer user_data);

// update the label in the menu button (mbutton_devices) accordingly
void display_selected_device();

// bound callback methods to respective signals
void setupCallbacks();

// initialize all the GTK widgets
void initializeWidgets();

// close gui, notify pmalb loop
void close_gui();


static void button_send_clicked(GtkWidget *widget, gpointer *data);

static void destroy_qrcode_popup(GtkWidget *widget, gpointer *data);

void button_add_device_clicked(GtkWidget *widget, gpointer *data);



//  MANAGING GUI


void notify_new_device_discovered() {
	return;

	GNotification *notification = g_notification_new("new-device");
	// g_notification_set_icon(notification, g_icon_new_for_string("phone", NULL));
	g_notification_set_body(notification, "A new device was discovered");

	if (app == NULL) {
		log_error("app is null");
	}


	g_application_send_notification(
		G_APPLICATION(app),
		NULL,
		notification
	);
}





static void activate(GtkApplication *app , gpointer data) {
	// load the UI

	if (gui_run) {
		gtk_widget_show((GtkWidget*) guiui.window);
	};
}

void initializeWidgets() {
	guiui.window = GTK_WINDOW(gtk_builder_get_object(guiui.builder, "mainwindow"));
	guiui.qrcode_eventbox = GTK_WIDGET(gtk_builder_get_object(guiui.builder, "qrcode_eventbox"));
	guiui.button_send = GTK_BUTTON(gtk_builder_get_object(guiui.builder, "button_send"));
	guiui.entry_url = GTK_ENTRY(gtk_builder_get_object(guiui.builder, "entry_url"));
	guiui.gswitch = GTK_WIDGET(gtk_builder_get_object(guiui.builder, "switch_server"));
	guiui.image_qrcode = GTK_IMAGE(gtk_builder_get_object(guiui.builder, "image_qrcode"));
	guiui.box_device_list = GTK_BOX(gtk_builder_get_object(guiui.builder, "box_device_list"));
	guiui.mbutton_devices = GTK_MENU_BUTTON(gtk_builder_get_object(guiui.builder, "mbutton_devices"));
	guiui.popover_devices = GTK_POPOVER(gtk_builder_get_object(guiui.builder, "popover_devices"));
	guiui.label_selected_device  = GTK_LABEL(gtk_builder_get_object(guiui.builder, "label_selected_device"));
	guiui.qrcode_popup = GTK_DIALOG(gtk_builder_get_object(guiui.builder, "qrcode_popup"));
	guiui.label_qrcode = GTK_LABEL(gtk_builder_get_object(guiui.builder, "label_qrcode_text"));
	guiui.button_add_device = GTK_BUTTON(gtk_builder_get_object(guiui.builder, "button_add_device"));

	gtk_builder_connect_signals(guiui.builder, &guiui);
}

void setupCallbacks() {
	g_signal_connect(guiui.window, "delete-event", G_CALLBACK(close_gui), NULL);
	g_signal_connect(guiui.qrcode_eventbox, "size-allocate", G_CALLBACK(qrcode_resize), NULL);
	g_signal_connect(guiui.button_send, "clicked", G_CALLBACK(button_send_clicked), guiui.entry_url);
	g_signal_connect(guiui.gswitch, "state-set", G_CALLBACK(toggle_server), NULL);
	g_signal_connect(guiui.button_add_device, "clicked", G_CALLBACK(button_add_device_clicked), NULL);
	g_signal_connect(app, "activate", G_CALLBACK(activate) , NULL);
	g_signal_connect(guiui.qrcode_popup, "delete-event", G_CALLBACK(destroy_qrcode_popup), NULL);
}


static void button_send_clicked(GtkWidget *widget, gpointer *data) {
	return;
}


void button_add_device_clicked(GtkWidget *widget, gpointer *data) {
	log_debug("send button clicked");

	// fill widgets with content
	const char *pairing_info = create_pairing_info();
	guiui.pb_qrcode = generate_qrcode_pixbuf(pairing_info);

	// int scaler = window_width / gdk_pixbuf_get_height(guiui.pb_qrcode);
	// guiui.pb_qrcode = scale_pixbuf(guiui.pb_qrcode, scaler-2);

	gtk_image_set_from_pixbuf(guiui.image_qrcode, guiui.pb_qrcode);
	gtk_widget_set_size_request((GtkWidget *) guiui.image_qrcode, MIN_QRCODE_SIZE, MIN_QRCODE_SIZE);
	gtk_label_set_text(guiui.label_qrcode, pairing_info);
	// free(pairing_info);

	gtk_dialog_run(GTK_DIALOG(guiui.qrcode_popup));
}


static void destroy_qrcode_popup(GtkWidget *widget, gpointer *data) {
	gtk_widget_hide(GTK_WIDGET(guiui.qrcode_popup));
}


void update_box_device_list() {
	json_t *jsn_phones = get_phones();

	if (jsn_phones == NULL) {
		log_info("no phones found");
		return;
	}

	size_t index;
	json_t *value;
	GtkRadioButton *new_device;
	static GtkRadioButton *last_device;

	json_array_foreach(jsn_phones, index, value) {
		// get phone name and id
		const char *device_name = json_string_value(json_object_get(value, PHONE_NAME));
		char *tooltip = malloc(20);
		sprintf(tooltip, "%d", (int) json_integer_value(json_object_get(value, PHONE_ID)));

		// create radio button for each phone, id as tooltip
		new_device = GTK_RADIO_BUTTON(gtk_radio_button_new_with_label(NULL, device_name));
		gtk_radio_button_join_group(new_device, last_device);
		gtk_widget_set_tooltip_text(GTK_WIDGET(new_device), tooltip);
		gtk_widget_set_direction(GTK_WIDGET(new_device), GTK_TEXT_DIR_RTL);
		g_signal_connect(new_device, "toggled", G_CALLBACK(select_device), NULL);

		// pack at start, save last_device
		gtk_box_pack_start(guiui.box_device_list, GTK_WIDGET(new_device), false, true,	0);
		last_device = new_device;
		log_info("Inserted device %s with id %s", device_name, tooltip);
	}

	gtk_widget_show_all(GTK_WIDGET(guiui.box_device_list));
	free(jsn_phones);

	log_info("listbox updated");
}

void display_selected_device() {
	struct Phone phone;
	// index 0, as selected element in combobox is always at index 0 in the array
	if (get_phone(&phone, 0)) {
		gtk_label_set_text(guiui.label_selected_device, phone.name);
	} else {
		gtk_label_set_text(guiui.label_selected_device, "Device List");
	}
}

void update_qrcode(char *new_content, double multiplier) {
	// generate qrcode, scale it, display it
	if (new_content == NULL) // if non given, create qr code
		 (generate_qrcode_pixbuf(new_content));

	GdkPixbuf *buf = scale_pixbuf(guiui.pb_qrcode, multiplier);
	log_debug( "new pixbuf: %dx%d", gdk_pixbuf_get_height(buf), gdk_pixbuf_get_width(guiui.pb_qrcode));
	gtk_image_set_from_pixbuf(guiui.image_qrcode, buf);
}

GdkPixbuf *generate_qrcode_pixbuf(const char *text) {
	// generate qr-code (binary array)
	uint8_t qrcode[qrcodegen_BUFFER_LEN_MAX];
	uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];
	bool ok = qrcodegen_encodeText(
		text,
		tempBuffer,
		qrcode,
		qrcodegen_Ecc_HIGH,
		qrcodegen_VERSION_MIN,
		qrcodegen_VERSION_MAX,
		qrcodegen_Mask_AUTO,
		true
	);

	if (!ok) {
		return NULL;
	}

	// convert binary array into RGB array
	int size = qrcodegen_getSize(qrcode);
	guchar *rgb_qrcode = (guchar*) malloc(size*size*CHANNELS);
	int index = 0;
	for (int y = 0; y < size; y++) {		    // iterate through each row (y-axis)
		for (int x = 0; x < size; x++) {		// iterate through each column (x-axis)
			for (int rgb = 0; rgb < CHANNELS; rgb++) {  // iterate through each channel (rgb)
				rgb_qrcode[index+rgb] = qrcodegen_getModule(qrcode, x, y) ? BLACK : WHITE;
			}
			index += 3;
		}
	}

	return gdk_pixbuf_new_from_data(
		rgb_qrcode,
		GDK_COLORSPACE_RGB,
		0,
		8,
		size,
		size,
		size*CHANNELS, // 3 color channels/sample
		NULL,
		NULL
	);
}

GdkPixbuf *scale_pixbuf(GdkPixbuf *pixbuf, double multiplier) {
	int new_wh = round(41 * multiplier);
	log_debug( "multiplier: %f / new_wh: %d", multiplier, new_wh);
	return gdk_pixbuf_scale_simple(
		pixbuf,
		new_wh,
		new_wh,
		GDK_INTERP_NEAREST
	);
}



void close_gui() {
	send_ipc_message(IPC_HIDE_GUI);
	gtk_widget_hide((GtkWidget*) guiui.window);
	gui_run = false;
}


/**
 * Initialize the gui and start gtk_main()
 * This is blocking and should be started as a separate thread
 */
void *init_gtk_application() {
	// unpack parameters
	// struct Params *params_p = (struct Params *) void_p;
	// struct Params params = *params_p;
	// int argc    = params.argc;
	// char **argv = params.argv;

	// just an initial state
	gui_run = false;

	log_debug("creating gtk_application");
	app = gtk_application_new("org.pauni.pmalb", G_APPLICATION_FLAGS_NONE);
	// app = gtk_application_new("net.0x000.pmalb", G_APPLICATION_FLAGS_NONE);
	// log_debug("OK");

	gtk_init(0, NULL);

	log_debug("load gui file");
	guiui.builder = gtk_builder_new_from_file(PATH_GUI);

	log_debug("created builder");

	initializeWidgets();

	// save current size of window
	// maybe no necessary
	// gtk_window_get_size((GtkWindow *) guiui.window, &window_width, &window_height);

	// log_debug("qrcode size: %d, scaler: %d", gdk_pixbuf_get_height(guiui.pb_qrcode), scaler);
	// display_selected_device();

	// setup callback after window is populated
	setupCallbacks();

	gtk_application_add_window(app, (GtkWindow *) guiui.window);

	gui_run = true;
	// g_object_unref(app);

	log_info("run application");
	g_application_run(G_APPLICATION(app), 0 , NULL);


	// notify_new_device_discovered();

	log_info("gtk main loop");
	gtk_main();
	return NULL;
}


void *show_gui() {
	return NULL;
}





//  CALLBACKs

bool send_link(GtkWidget *widget, GtkEntry *entry_url) {
	log_warn("unimplemented");
	return true;
	// const char *url = gtk_entry_get_text(entry_url);
	// int url_length = gtk_entry_get_text_length(entry_url);

	// if (url_length == 0) {
	// 	log_info("entry_link empty. Leaving  function");
	// 	return false;
	// }

	// struct Phone phone;
	// // index 0, as selected element in combobox is always at index 0 in the array
	// if (get_phone(&phone, 0)) {
	// 	log_debug("%s", phone.ip);
	// 	pass_to_phone(phone, LINK_REQUEST, url, url_length);
	// 	return true;
	// }
	// return false;
}

void select_device(GtkToggleButton *togglebutton, gpointer user_data) {
	static int selected_id;

	// negate '!' correct here? @futureMe
	if(!gtk_toggle_button_get_active(togglebutton)) {
		// we don't care about unselected-signal
		log_debug("unselected. Leave function");
		return;
	}

	// GtkLabel *label = GTK_LABEL(gtk_bin_get_child(GTK_BIN(togglebutton)));
	const char *name = gtk_widget_get_name(GTK_WIDGET(togglebutton));
	int id = strtol(gtk_widget_get_tooltip_text(GTK_WIDGET(togglebutton)), NULL, 10);
	log_info("Selected device %s with id %d", name, id);

	if (selected_id != id) {
		set_selected_phones(id);
		display_selected_device();
	} else	// if selected entry clicked again, close popover
		gtk_button_clicked(GTK_BUTTON(guiui.mbutton_devices));

	selected_id = id;
}


gboolean toggle_server(GtkSwitch *widget, gboolean state, gpointer user_data) {
	log_trace("requesting server state change");
	if (state) {
		send_ipc_message(IPC_START_SERVER);
		log_trace("start pmalb server sent");
	} else {
		send_ipc_message(IPC_STOP_SERVER);
		log_trace("stop pmalb server sent");
	}

	// method type must be gboolean and must return false to trigger native
	// GtkSwitch method, read documentation of GtkSwitch: Signals
	return false;
}

gboolean debug (GtkWidget *widget, GdkRectangle *allocation, gpointer user_data) {
	static int i=0;

	GtkRadioButton *child;
	static GtkRadioButton *last_button;
	i++;
	char *name = malloc(21);
	char *tip = malloc(10);
	sprintf(name, "MyDevice %d", i);
	sprintf(tip, "%d", i);

	child = (GtkRadioButton *) gtk_radio_button_new_with_label(NULL, name);
	gtk_widget_set_direction(GTK_WIDGET(child), GTK_TEXT_DIR_RTL);

	if (i>1)
		gtk_radio_button_join_group (child, last_button);


	gtk_widget_set_tooltip_text(GTK_WIDGET(child), tip);
	gtk_box_pack_start(guiui.box_device_list, GTK_WIDGET(child), false, true, 0);
	gtk_widget_show_all(GTK_WIDGET(guiui.box_device_list));

	last_button = child;
	return true;
}

void qrcode_resize(GtkWidget *widget, GdkRectangle *allocation, gpointer user_data) {
	static GdkPixbuf *pb_scaled;
	int img_size;
	int new_width  = gtk_widget_get_allocated_width((GtkWidget *) widget);
	int new_height = gtk_widget_get_allocated_height((GtkWidget *) widget);

	// log_info("resize triggered");

	// check whether window size has changed
	if (window_width != new_width || window_height != new_height) {
		// unref previous pb_scaled (it's static)
		g_object_unref(pb_scaled);

		// assign smaller value to img_size
		(new_width<=new_height) ? (img_size = new_width) : (img_size = new_height);

		// introduce 10% padding by using only 90% of window-size for image
		img_size = (int) round(img_size*0.9);

		pb_scaled = gdk_pixbuf_scale_simple(guiui.pb_qrcode, img_size, img_size, GDK_INTERP_NEAREST);
		gtk_image_set_from_pixbuf(guiui.image_qrcode, pb_scaled);

		window_width  = new_width;
		window_height = new_height;

		// log_debug(
		// 	"New Pixbuf: %dx%d, Image height: %d",
		// 	img_size, img_size,
		// 	gtk_widget_get_allocated_height((GtkWidget *) guiui.image_qrcode)
		// );

		return;
	}

	log_debug("size not changed");
	return;
}




char *create_pairing_info() {
	uint32_t presecret = get_pairing_secret();
	log_debug("session preshared key: %x", presecret);

	unsigned char *pubkey = get_own_public_key();

	uint8_t pairing[16];
	pairing[0] = (presecret >> 24) & 0xff;
	pairing[1] = (presecret >> 16) & 0xff;
	pairing[2] = (presecret >> 8) & 0xff;
	pairing[3] = presecret;

	memcpy(&pairing[4], pubkey, 8);
	return hexencode(pairing, 12);
}

int gui_is_running() {
	return gui_run;
}
