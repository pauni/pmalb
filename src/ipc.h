/** \file */

#ifndef IPC_H_
#define IPC_H_

#include <stdbool.h>

// using GNOME naming scheme
typedef enum {
	IPC_SHOW_GUI,
	IPC_HIDE_GUI,
	IPC_START_SERVER,
	IPC_STOP_SERVER
} IpcMessage;


char* ipc_message_text(IpcMessage message);
bool is_single_instance();
void send_ipc_message(IpcMessage message);
void receive_ipc_message(IpcMessage* message, int buf_size);
void close_ipc_socket();

#endif
