#include <jansson.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <assert.h>

#include "log.h"
#include "pmalb.h"
#include "protocol.h"
#include "userdata.h"
#include "discovery.h"


/*
 *  This file has only one "public" function which takes the input
 *  and processes it. How it is processed is defined by this protocol.
 */


// PROTOTYPES
void open_link(json_t *url_obj);

void pair(json_t *auth_obj);


int handle_receive_file(int client, pmalb_proto_header_t *header) {
	log_info("handle incoming file...");
	sleep(1);
	log_info("not really");
	return 0;
}



int handle_remote_control(int client, pmalb_proto_header_t *header) {
	log_info("handle remote control request");

	return 0;
}






/// what ever this does
int handle_pairing(int client_fd, pmalb_proto_header_t *header) {
	unsigned char *payload_buffer = (unsigned char*) calloc(1, header->payload_size);
	size_t l = 0;

	if((l = read(client_fd, payload_buffer, header->payload_size)) < 0 ) {
		// reading high amount of data with one read() could lead to performance problems
		// todo: find out if it leads to performance problems and if so use buffering
		perror("something went wrong");
		exit(1);
	}

	assert(l == header->payload_size);


	struct sockaddr_in6 remote;
	socklen_t address_len = sizeof(remote);
	getpeername(client_fd, (struct sockaddr*) &remote, &address_len);

	char ip[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &remote.sin6_addr, ip, address_len);
	log_trace("got pair request from: %s", ip);

	struct disco_device *device = get_discovered_device_by_ip(*(struct in6_addr*) &remote);

	// assert for development
	assert(device != NULL);

	if (device == NULL) {
		log_debug("sender is not known. Can't verify message sender");
		log_warn("ignoring pair request from unknown sender");
		close(client_fd);
		return 0;
	}

	unsigned char *pubkey = device->public_key;
	unsigned char *seckey = get_own_secret_key();

	log_debug("verifying with %s", hexencode(pubkey, PUBKEYLEN));
	log_debug("decrypting with %s", hexencode(seckey, SECKEYLEN));


	unsigned char nonce[crypto_box_NONCEBYTES] = {0};
	memset(nonce, 0, crypto_box_NONCEBYTES);

	unsigned char *decrypted = calloc(sizeof(unsigned char), l - crypto_box_MACBYTES);

	log_info("read %d bytes", l);

	if (crypto_box_open_easy(decrypted, payload_buffer, header->payload_size, nonce, pubkey, seckey) != 0) {
		log_error("someone fucked up");
		close(client_fd);
		return 1;
	}


	return 0;
}



/// The message dispatcher takes a header and the socket and passes the two to the
/// corresponding function, that handles the message typea
void protocol_message_dispatcher(int client_fd, pmalb_proto_header_t *header) {
	/// todo: more complex return codes
	int (*handler_f)(int, pmalb_proto_header_t*);
	log_trace("matching against %0d", header->message_type);

	switch (header->message_type) {
		case PMALB_TYPE_FILE:
			handler_f = &handle_receive_file;
			break;
		case PMALB_TYPE_REMOTECTL:
			handler_f = &handle_remote_control;
			break;
		case PMALB_TYPE_PAIRNG:
			handler_f = &handle_pairing;
			break;
		default:
			log_warn("message type is not implemented");
			return;
	}


	if (handler_f(client_fd, header) < 0) {
		log_error("handler function returned error. See previous log messages");
	}


//     json_t *root;
//     json_error_t error;
//     root = json_loads((const char*) input->buffer, input->size, &error);
//     if (!root) {
//	   log_error("error while parsing json at line %d: %s\n", error.line, error.text);
//		exit(1);
//     }

//     const char *request_type = json_string_value(json_object_get(root, REQUEST_TYPE));
//     log_debug( "process_input: %s", request_type);

//     if (compare(request_type, LINK_REQUEST))
//	   open_link(root);
//     else if (compare(request_type, PAIR_REQUEST))
//	   pair(root);
}

void open_link(json_t *root) {
    if (json_is_object(root)) {
	char *url = (char*) json_string_value(json_object_get(root, DATA));

	// create url open command
	char *command = (char*) malloc(50);			    // allocate memory for buffer
	log_debug( "opened url: %s", url);
	strcpy(command, "xdg-open ");				    // copy xdg-open to start of array
	strcat(command, url);					    // concatenate command and url
	system(command);					    // execute command
	free  (command);
	free  (url);
    }
}

int encapse_message(char **buf, const char *request_type, const void *data) {
    json_t *encapsed = json_object();
    json_object_set(encapsed, REQUEST_TYPE, json_string(request_type));
    json_object_set(encapsed, DATA, json_string(data));

    // create json string and get size
    char *json_str = json_dumps(encapsed, JSON_ENCODE_ANY);
    int size = (int) json_dumpb(encapsed, NULL, 0, 0);

    // create string with size prefix and json string
    *buf = malloc(MSG_LEN_FIELD + size);
    sprintf(*buf, "%03d", size);
    strcat(*buf, json_str);
    free(encapsed);
    free(json_str);
    log_debug("%s", *buf);

    return MSG_LEN_FIELD + size;
}
