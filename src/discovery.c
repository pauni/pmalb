#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <jansson.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sodium.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "net.h"
#include "discovery.h"
#include "log.h"
#include "pmalb.h"
#include "protocol.h"
#include "userdata.h"
#include "gui.h"




struct discovery_service_status disco_status = {0};




/// This needs a rewrite
int send_beacon(struct sockaddr_in6 *dst, bool multicast) {
	// struct discovery_beacon = {
	// 	.multicast = is_mulicast_address(dest),
	// 	.public = key
	// };

	uint8_t flags = 0;
	unsigned char signature[SIGNATURELEN] = {0};
	unsigned char *sk = get_own_secret_signkey();

	// if (is_multicast_address(dst->sin6_addr)) {
	if (multicast) {
		log_trace("Set multicast flag");
		flags = flags | FLAG_MULTICASTED;
	}


	char a[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, (struct sockaddr*) &dst->sin6_addr, a, INET6_ADDRSTRLEN);


	unsigned char *buffer = (unsigned char*) malloc(DISCOVERY_BEACON_SIZE);

	buffer[0] = PMALB_PROTO_VERSION;
	buffer[1] = flags;
	buffer[2] = htons(PMALB_UDP_TYPE_DISCOVERY);

	unsigned char *pubkey = get_own_public_key();

	crypto_sign_detached(signature, NULL, pubkey, PUBKEYLEN, sk);


	memcpy(&buffer[4], signature, SIGNATURELEN);
	memcpy(&buffer[4 + SIGNATURELEN], pubkey, PUBKEYLEN);

	log_debug("sending beacon: multicast=%s dest=%s scope=%d flags=%x", multicast?"true":"false", a, dst->sin6_scope_id, (char) buffer[1]);

	if (!multicast) {
		int s;
		if ((s = socket(AF_INET6, SOCK_DGRAM, 0)) == -1) {
			log_error("unable to start discovery responder: %s", strerror(errno));
			exit(1);
			// return -1;
		};

		if(sendto(s, buffer, DISCOVERY_BEACON_SIZE, 0,  (struct sockaddr*) dst, sizeof(*dst)) == -1) {
			log_error("can't send beacon: %s", strerror(errno));
			return -1;
		}
	}
	else {
		int *s = get_udp_socket();

		struct ifaddrs *ifaddr, *ifa;
		if (getifaddrs(&ifaddr) == -1) {
			log_error("unable to get interfaces: %s", strerror(errno));
			exit(EXIT_FAILURE);
		}

		for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
			if (ifa->ifa_addr == NULL) {
				continue;
			}


			if (ifa->ifa_addr->sa_family == AF_INET6) {
				int iface;
				if ((iface = if_nametoindex(ifa->ifa_name)) == 0) {
					log_error("can't get interface index: ", strerror(errno));
					continue;
				}


				if (setsockopt(*s, IPPROTO_IPV6, IPV6_MULTICAST_IF, &iface, sizeof iface) != 0) {
					log_error("failed to set sending interface to %s: %s", ifa->ifa_name, strerror(errno));
					exit(EXIT_FAILURE);
				}


				// log_debug("set out interface to %s",ifa->ifa_name);

				if(sendto(*s, buffer, DISCOVERY_BEACON_SIZE, 0,  (struct sockaddr*) dst, sizeof(*dst)) == -1) {
					log_error("can't send on %s beacon: %s", ifa->ifa_name, strerror(errno));
					continue;
				}
			}
		}

		freeifaddrs(ifaddr);
		log_debug("beacon sent");
	}

	free(buffer);
	return 0;
}


// Todo: move device adding code to userdata.c
void recv_beacon(pmalb_udp_packet_t *packet) {
	time_t timestamp;
	unsigned char *signature;
	unsigned char *public_key;

	time(&timestamp);

	assert(packet->len == DISCOVERY_BEACON_SIZE);

	if (packet->len != DISCOVERY_BEACON_SIZE) {
		log_error("publickey has unexpected size");
		return;
	};


	signature = &packet->payload[0];
	public_key = &packet->payload[SIGNATURELEN];


	struct paired_device *source_device = get_paired_device(public_key);

	if (source_device != NULL) {
		log_debug("device is known (paired). checking signature");
		// device is known; verifying packet

		bool success = crypto_sign_verify_detached(
			signature,
			public_key,
			PUBKEYLEN,
			*source_device->public_key
		);

		if (!success) {
			log_debug("signature check failed");
			// the package was tampered with; do not update the ip
			return;
		};

		log_debug("signature is valid 👍");
	};


	log_debug("checking signature");



	char ip[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &packet->remote->sin6_addr, ip, INET6_ADDRSTRLEN);


	log_info("received beacon from: %s ", ip);
	log_debug("remote device is %s", hexencode(public_key, PUBKEYLEN));


	// check if device is already known
	if (disco_status.devices == NULL) {
		log_info("Discovery table is empty. Adding device");
		struct disco_device* new_device = malloc(sizeof(struct disco_device));
		new_device->remote_addr = packet->remote->sin6_addr;
		new_device->last_seen = timestamp;
		new_device->id = 0; // todo

		memcpy(new_device->public_key, public_key, PUBKEYLEN);
		new_device->next_device = NULL;
		disco_status.devices = new_device;


		notify_new_device_discovered();
	}
	else {
		for (struct disco_device* dd = disco_status.devices; dd != NULL; dd = dd->next_device) {
			log_debug("check table - current: %s", hexencode(public_key, PUBKEYLEN));
			log_debug("check table - new    : %s", hexencode(dd->public_key, PUBKEYLEN));

			if ( memcmp(&dd->public_key, &public_key, PUBKEYLEN) ) {
				log_trace("device is known");

				char ip[INET6_ADDRSTRLEN];
				inet_ntop(AF_INET6, &packet->remote->sin6_addr, ip, INET6_ADDRSTRLEN);
				log_trace("updating ip to: %s", ip);
				// update last seen and remote address
				dd->last_seen = timestamp;
				dd->remote_addr = packet->remote->sin6_addr;
				break;
			}
			else {
				log_trace("not found yet");

				if (dd->next_device == NULL) {
					log_trace("we are at the end. appending device");
					struct disco_device* new_device = malloc(sizeof(struct disco_device));
					new_device->remote_addr = packet->remote->sin6_addr;
					new_device->last_seen = timestamp;
					new_device->id = 0; // todo

					memcpy(new_device->public_key, public_key, PUBKEYLEN);
					new_device->next_device = NULL;

					dd->next_device = new_device;
				};
			}
		}
	}



	// device is not known or

	// check if we need to reply
	if (packet->flags.multicasted) {
		log_trace("packet was multicasted. sending reply");
		struct sockaddr_in6 dst;
		dst.sin6_family = AF_INET6;
		dst.sin6_port = htons(PMALB_PORT);
		dst.sin6_addr = packet->remote->sin6_addr;
		dst.sin6_scope_id = packet->remote->sin6_scope_id;

		send_beacon(&dst, false);
	}

	return;
}


/**
 * Returns a linked list of the discoved devices
 */

struct disco_device* get_discovered_devices(void) {
	return disco_status.devices;
}





/**
 * start the discovery responde
 * @Todo stop when disco_status.running is set to `false`
 *
 * @param public key to multicast
 */
void *start_discovery_service() {
	// if ((s = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
	// 	log_fatal("can't create socket %s", strerror(errno));
	// 	exit(1);
	// }


	log_trace("start discovery thread");

	unsigned char *pubkey = get_own_public_key();


	log_info("advertising %s on multicast group", hexencode(pubkey, PUBKEYLEN));


	struct sockaddr_in6 dst;
	dst.sin6_family = AF_INET6;
	dst.sin6_port = htons(PMALB_PORT);
	inet_pton(AF_INET6, MULICAST_GROUP, &dst.sin6_addr);


	char a[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, (struct sockaddr*) &dst.sin6_addr, a, INET6_ADDRSTRLEN);
	log_trace("call beacon sender with to: %s", a);


	while (true) {
		send_beacon(&dst, true);
		sleep(BEACON_INTERVAL);
	}


	// pthread_create(
	// 	&disco_status.thread,
	// 	NULL,
	// 	discovery_responder_loop,
	// 	s
	// );
}


void parse_discovery_beacon(pmalb_udp_packet_t *packet, discovery_beacon_t *beacon) {
	if (packet->len != DISCOVERY_BEACON_SIZE) {
		log_fatal("discovery beacon packet does not have the expected size");
		exit(1);
	}

	beacon->multicast = (bool) packet->flags.multicasted;
	memcpy(beacon->public, packet->payload, PUBKEYLEN);
}


struct disco_device *get_discovered_device_by_ip(struct in6_addr addr) {
	// check if device is already known
	char ip[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &addr, ip, INET6_ADDRSTRLEN);
	log_trace("looking for ip address: %s", ip);

	for (struct disco_device* dd = disco_status.devices; dd != NULL; dd = dd->next_device) {
		inet_ntop(AF_INET6, &dd->remote_addr, ip, INET6_ADDRSTRLEN);
		log_trace("matching against: %s", ip);
		if ( memcmp(&dd->remote_addr, &addr, 16) ) {
			log_trace("found public key");
			return dd;
		}
	}

	return NULL;
}
