/** \file */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#define MSG_LEN_FIELD 40


// JSON KEYS FOR THE DATA EXCHANGE OVER TCP

#define REQUEST_TYPE "request_type"
#define DATA	      "data"

#define PAIR_REQUEST  "pair_request"
#define LINK_REQUEST  "link_request"
#define PICT_REQUEST  "pict_request"

#define PHONE_ID     "phone_id"
#define PHONE_IP     "phone_ip"
#define PHONE_PORT   "phone_port"
#define PHONE_NAME   "phone_name"
#define PHONE_PUBKEY "phone_pubkey"



/// Used when the phone wants to send a file
#define PMALB_TYPE_FILE 5
/// unused
#define PMALB_TYPE_BINARY 2
/// remote control request
#define PMALB_TYPE_REMOTECTL 3
/// Text
#define PMALB_TYPE_TEXT 4
/// Pairing
#define PMALB_TYPE_PAIRNG 1



struct Phone {
    int	 *id;
    char *ip;
    unsigned char *pubkey;
    char *name;
    int	  port;
};


#include "net.h"

enum proto_message_type {
	MessageTypeFile = PMALB_TYPE_FILE,
	MessageTypeBinary = PMALB_TYPE_BINARY,
	MessageTypeRemoteCtlRequest = PMALB_TYPE_REMOTECTL
};

void protocol_message_dispatcher(int client_fd, pmalb_proto_header_t *header);
int encapse_message(char **buf, const char *request_type, const void *data);

#endif
