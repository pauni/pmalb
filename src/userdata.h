/*! \file */

#ifndef USERDATA_H_
#define USERDATA_H_


#include <jansson.h>
#include <stdbool.h>
#include <sodium.h>
#include "protocol.h"

#define PREFERENCES_FILE "./pmalb.json"
#define PUBKEYLEN (crypto_sign_PUBLICKEYBYTES + crypto_box_PUBLICKEYBYTES)
#define SECKEYLEN (crypto_sign_SECRETKEYBYTES + crypto_box_SECRETKEYBYTES)



struct paired_device {
	struct paired_device *next;
	char* name;
	unsigned char* public_key[PUBKEYLEN];
	uint64_t last_seen;
	struct in6_addr last_ip_addr;
};



/// get the phone
bool get_phone(struct Phone *phone, int index);

json_t *get_phones();
bool save_phone(json_t *phone);
bool set_selected_phones(int id);
void init_devicemanager();



/**
 * get a paired device
 * @param a pointer to a public key
 * @return pointer to `struct paired_device` or `NULL`
 */
struct paired_device *get_paired_device(unsigned char *pubkey);

/// save current configuration
int save_config();

/// Todo
unsigned char* get_public_key_as_hex();


/**
 * gets own public key. The caller must have
 * allocated enough space at `key`!
 * @param pointer to where the key should go
 */
unsigned char *get_own_public_key();
unsigned char *get_own_public_signkey();
unsigned char *get_own_public_cryptokey();



unsigned char *get_own_secret_key();
unsigned char *get_own_secret_signkey();
unsigned char *get_own_secret_cryptokey();


/**
 * This function will allocate enough memory
 * and set out to the appropriate memory location
 *
 * @param nullterminated string
 * @param pointer to a buffer
 *
 */
size_t hexdecode(const char *hex, unsigned char **out);


/**
 * will allocate the needed space and returns
 * a pointer to the location.
 *
 * @param some binary data
 * @param length of the data
 */
char *hexencode(unsigned char *bin, size_t len);


/**
 * returns the preshared secret for this session
 */
uint8_t get_pairing_secret(void);


/**
 * Regenerate pairing secret
 * should be regenerated after a new device was paired
 */
void regenerate_pairing_secret(void);


#endif
