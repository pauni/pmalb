/** \file */

#ifndef GUI_H_
#define GUI_H_

int gui_is_running();
void *show_gui();


/**
 * initialize the application and setup all dbus signals
 */
void *init_gtk_application();


// updates the list of added devices
void update_box_device_list();

// insert (newly added?) phone into listbox (list_devices)
void git();

#include "discovery.h"

/**
 * notify gui about a new device
 */
void notify_new_device_discovered();


#endif
