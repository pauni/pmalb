#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <ifaddrs.h>
#include <jansson.h>
#include <jansson.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sodium.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

#include "discovery.h"
#include "log.h"
#include "net.h"
#include "pmalb.h"
#include "protocol.h"



int server_run = 0;
int server_fd, client_fd, err;
int msg_len;
char buf_msg_len[MSG_LEN_FIELD];
struct sockaddr_in6 server, client;

int udp_s;



void server_loop();

void *udp_server_loop(int *s);


/// deserializes the `pmalb_proto_header`
pmalb_proto_header_t *deserialize_header(unsigned char* buffer);



/** \file
 * ```
 *	0	       8	      16	      24	      32
 *	┌─────────────────────────────────────────────────────────────┐
 *  0	| payload size						      |
 *  4	|							      |
 *	├──────────────┬──────────────┬───────────────────────────────┤
 *  8	| Flags	       | Version      | Type			      |
 *	└──────────────┴──────────────┴───────────────────────────────┘
 * ```
 */




/// errors that are caused by network errors should exit with 4

/**
 *  TCP server
 */

void start_server() {
	// thanks to "mafintosh" for the code: "https://github.com/mafintosh/echo-servers.c/blob/master/tcp-echo-server.c"
	server_fd = socket(AF_INET6, SOCK_STREAM, 0);
	if (server_fd < 0) {
		log_fatal("Could not create socket: %s", strerror(errno));
		exit(4);
	}

	// configure server addr
	server.sin6_family = AF_INET6;
	server.sin6_port = htons(PMALB_PORT);
	server.sin6_addr = in6addr_any;

	// configure server socket
	int opt_val = 1;
	setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &opt_val, sizeof opt_val);

	// bind server on the addr

	if (bind(server_fd, (struct sockaddr *)&server, sizeof(server)) != 0) {
		log_fatal("can't bind to socket: %s", strerror(errno));
		exit(4);
	}

	// start listening
	if (listen(server_fd, PMALB_PORT) != 0) {
		log_fatal("can't listen on socket: %s", strerror(errno));
	}

	log_debug("TCP Server is listening on port: %d", PMALB_PORT);
	server_run = 1;
	server_loop();

	return;
}




void server_loop() {
	while (server_run) {
		log_debug("Waiting for new client...");
		socklen_t client_len = sizeof(client);

		// waiting for client
		client_fd = accept(server_fd, (struct sockaddr *) &client, &client_len);
		if (client_fd < 0) {
			log_fatal("Could not establish client connection: ", strerror(errno));
			close_server();
			exit(4);
		}


		// allocate 12 bytes. Thats the size of our header
		unsigned char *buffer = (unsigned char*) malloc(12);

		int bytes_read = read(client_fd, buffer, 12);
		log_debug("bytes read: %d", bytes_read);

		pmalb_proto_header_t *header = deserialize_header(buffer);

		log_debug("protocol version: %02d", header->version);
		log_debug("payload size: %-5lli", header->payload_size);
		log_debug("flags: %02x", *(uint8_t*) &header->flags);
		log_debug("message type: %02x", *(uint16_t*) &header->message_type);


		// if(read(client_fd, payload_buffer, header->payload_size) < 0 ) {
		// 	// reading high amount of data with one read() could lead to performance problems
		// 	// todo: find out if it leads to performance problems and if so use buffering
		// 	perror("something went wrong");
		// 	exit(1);
		// }


		// struct pmalb_proto_payload payload = {
		// 	.size = header->payload_size,
		// 	.buffer = payload_buffer,
		// };


		protocol_message_dispatcher(client_fd, header);

		log_debug("Close conection");
		close(client_fd);
	}
}


/// deserializes the `pmalb_proto_header`
pmalb_proto_header_t *deserialize_header(unsigned char* buffer) {
	// just make sure everything is clear
	pmalb_proto_header_t *header = malloc(sizeof *header);

	log_trace("payload size on wire:");
	log_trace("[0] %02x", buffer[0]);
	log_trace("[1] %02x", buffer[1]);
	log_trace("[2] %02x", buffer[2]);
	log_trace("[3] %02x", buffer[3]);
	log_trace("[4] %02x", buffer[4]);
	log_trace("[5] %02x", buffer[5]);
	log_trace("[6] %02x", buffer[6]);
	log_trace("[7] %02x", buffer[7]);

	// convert big endian to little endian
	header->payload_size = ((uint64_t) buffer[0]) |
							((uint64_t) buffer[1]) << 8 |
							((uint64_t) buffer[2]) << 16 |
							((uint64_t) buffer[3]) << 24 |
							((uint64_t) buffer[4]) << 32 |
							((uint64_t) buffer[5]) << 40 |
							((uint64_t) buffer[6]) << 48 |
							((uint64_t) buffer[7]) << 56;

	// maybe this does'nt work
	header->flags = *(pmalb_proto_flags_t*) &buffer[8];
	// extract version
	header->version = ((uint8_t) buffer[9]);
	header->message_type = ntohs(*(uint16_t*) &buffer[10]);

	return header;
}


/// check if the server is running
int server_is_running() {
	return server_run;
}


/**
 * @brief close connection to server
 *  Close the server connection
 */
void close_server() {
	close(server_fd);
	server_run = 0;
}


/**
 *  TCP client
 */
bool phone_is_reachable(int *p_sock, struct sockaddr_in addr) {
	/*
	 *  Thanks to HectorLasso for providing an example for checking a connection non-blocking
	 *  developerweb.net/viewtopic.php?id=3196EINPROGRESS
	 */

	bool is_reachable = false;

	int sock = *p_sock;
	int valopt;
	long arg;
	struct timeval tv;
	socklen_t sock_length;
	fd_set set;

	// set socket to non-blocking
	if ((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
		log_error("%s", strerror(errno));
		is_reachable = false;
	}

	arg |= O_NONBLOCK;
	tv.tv_sec = 2;
	tv.tv_usec = 1000000;
	if (fcntl(sock, F_SETFL, arg) < 0) {
		log_error("%s", strerror(errno));
		is_reachable = false;
	}

	if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
		if (errno != EINPROGRESS) {
			log_error("%s", strerror(errno));
			is_reachable = false;
		}

		log_debug("waiting 2 seconds to reconnect");
		FD_ZERO(&set);
		FD_SET(sock, &set);

		int res = select(sock+1, NULL, &set, NULL, &tv);
		if (res < 0 && errno != EINTR) {
			log_error("%s", strerror(errno));
			is_reachable = false;
		}
		else if (res > 0) {
			sock_length = sizeof(int);

			// get error of connect() completion status and print it
			if (getsockopt(sock, SOL_SOCKET, SO_ERROR, (void*)(&valopt), &sock_length) < 0) {
				log_error("%s", strerror(errno));
				is_reachable = false;
			}

			if (valopt) {
				log_info("%s", strerror(errno));
				is_reachable = false;
			} else {
				// succesfully connected;
				is_reachable = true;
			}
		}
	}

	// Set to blocking mode again...
	if((arg = fcntl(sock, F_GETFL, NULL)) < 0) {
		log_error("%s", strerror(errno));
		exit(errno); // change to blocking fails mean the program will misbehave, cancle it.
	}

	arg &= (~O_NONBLOCK);
	if(fcntl(sock, F_SETFL, arg) < 0) {
		log_error("%s", strerror(errno));
		exit(errno);
	}

	log_info("phone is reachable: %d", is_reachable);
	return is_reachable;

}


/// this function has to be called 'pass' instead of 'send' due to the very nature of this program
int pass_to_phone(const struct Phone target, char *request_type, const char *msg, const int len) {
	// TODO: check if target server is online, because connect() would block program for way too long
	// use this as reference https://www.codeproject.com/tips/168704/how-to-set-a-socket-connection-timeout
	int sock;
	struct sockaddr_in phone_addr;

	phone_addr.sin_family = AF_INET;
	phone_addr.sin_port   = htons(2244);
	// char buf[16]; inet_pton(AF_INET, "192.168.178.35", buf); phone_addr.sin_addr.s_addr = buf;
	phone_addr.sin_addr.s_addr = inet_addr(target.ip);

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0) {
		log_error("could not bind socket: %s", strerror(errno));
		return 4;
	}

	if (!phone_is_reachable(&sock, phone_addr)) {
		log_info("phone's TCP server isn't reachable at: %s:%d", target.ip, target.port);
	}

	if (connect(sock, (struct sockaddr *) &phone_addr, sizeof(struct sockaddr_in)) < 0) {
		log_error("connecting stream socket: %s", strerror(errno));
		return 5;
	}

	char *out; // that's very bad
	int out_size = encapse_message(&out, request_type, msg);
	send(sock, out, out_size, 0);
	log_debug("output: %s", out);
	close(sock);
	return 0;
}


/// converts a char buffer to a disco_device
int deserialize_device(char *buffer, struct disco_device *new_device) {
	json_error_t error;
	json_t* device;
	device = json_loads(buffer, 0, &error);

	if (!json_is_object(device)) {
		log_warn("received multicast garbage");
		return 1;
	}


	json_t* name;
	if ((name = json_object_get(device, "name")) == NULL) {
		log_warn("no device name");
		return 1;
	}

	strcpy(new_device->name, json_string_value(name));
	return 0;
}


int compare_ipv6_addr(struct in6_addr *a, struct in6_addr *b) {
	return a->s6_addr == b->s6_addr;
}


void dispatch_udp_packet(pmalb_udp_packet_t *packet) {
	switch (packet->packet_type) {
		case PMALB_UDP_TYPE_DISCOVERY:
			log_info("received discovery packet");
			// discovery_beacon_t *beacon = malloc(sizeof(discovery_beacon_t));
			recv_beacon(packet);

			break;
		default:
			log_warn("packet type %d is not supported yet", packet->packet_type);
			break;
	}
};




void prepare_udp_socket(int *s) {
	if ((*s = socket(AF_INET6, SOCK_DGRAM, 0)) == -1) {
		log_error("unable to start discovery responder: %s", strerror(errno));
		exit(1);
		// return -1;
	};



	struct sockaddr_in6 addr;

	addr.sin6_family = AF_INET6;
	addr.sin6_port = htons(PMALB_PORT);
	addr.sin6_addr = in6addr_any;
	addr.sin6_flowinfo = 0;
	addr.sin6_scope_id = 0;


	char a[INET6_ADDRSTRLEN];
	log_info("binding to %s", inet_ntop(AF_INET6, &addr.sin6_addr, a, INET6_ADDRSTRLEN));

	if(bind(*s, (struct sockaddr*) &addr, sizeof addr) == -1) {
		log_error("cant't bind to address: %s", strerror(errno));
		exit(EXIT_FAILURE);
	};


	int loop = 0;

	if (setsockopt(*s, IPPROTO_IPV6, IPV6_MULTICAST_LOOP, &loop, sizeof(int)) == -1) {
		log_fatal("unable to enable loop prevention: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}


	struct ifaddrs *ifaddr, *ifa;

	if (getifaddrs(&ifaddr) == -1) {
		log_error("unable to get interfaces: %s", strerror(errno));
		exit(EXIT_FAILURE);
	}

	struct ipv6_mreq mgroup;
	inet_pton(AF_INET6, MULICAST_GROUP, &mgroup.ipv6mr_multiaddr);

	for (ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next) {
		if (ifa->ifa_addr == NULL) {
			continue;
		}

		if (ifa->ifa_addr->sa_family == AF_PACKET) {
			if ((mgroup.ipv6mr_interface = if_nametoindex(ifa->ifa_name)) == 0) {
				log_error("can't get interface index: ", strerror(errno));
			};

			if (setsockopt(*s, IPPROTO_IPV6, IPV6_ADD_MEMBERSHIP, (char*) &mgroup, sizeof mgroup) != 0) {
				log_error("unable to join mulicast group on %s: %s", ifa->ifa_name, strerror(errno));
				exit(EXIT_FAILURE);
			};

			log_info("joined multicast group on interface %s",ifa->ifa_name);
		}
	}




	freeifaddrs(ifaddr);
	assert(listen(*s, CONNECTION_QUEUE_SIZE) != 0);
}



/// start the server loop on udp socket
void *udp_server_loop(int *s) {

	log_debug("udp server loop started");


	while (true) {
		log_trace("waiting for udp packets");

		unsigned char* buf = (unsigned char*) calloc(sizeof(unsigned char), UDP_BUFFER_SIZE);
		struct sockaddr_in6 client_addr = {0};
		socklen_t l;
		ssize_t len;

		if ((len = recvfrom(*s, buf, UDP_BUFFER_SIZE, 0, (struct sockaddr*) &client_addr, &l)) < 0 ) {
			log_warn("error while receiving UDP datagram: %s", strerror(errno));
			continue;
		}

		char ip[INET6_ADDRSTRLEN];
		inet_ntop(AF_INET6, &client_addr.sin6_addr, ip, INET6_ADDRSTRLEN);
		log_info("received %d bytes from: %s in scope %d", len, ip, client_addr.sin6_scope_id);


		// if (memcmp(&client_addr.sin6_addr, &in6addr_loopback, sizeof(struct in6_addr))) {
		// 	log_trace("received packet from ourself. dropping");
		// 	continue;
		// }




		pmalb_udp_packet_t packet;

		packet.len = len;
		packet.remote = (struct sockaddr_in6*) &client_addr;
		packet.version = (uint8_t) buf[0];
		packet.flags = *(pmalb_udp_packet_flags_t*) &buf[1];

		packet.payload = malloc(len-4);
		memcpy(packet.payload, &buf[4], len-3);


		switch (packet.flags.encryption_type) {
			case PMALB_ENCRYPTION_TYPE_NONE:
				log_trace("received packet is unencrypted");
				packet.packet_type = ntohs(buf[2]);
				// todo
				break;
			case PMALB_ENCRYPTION_TYPE_ASYM:
				log_trace("received packet is symmetrically encrypted");
				log_error("stop processing packet here");
				// at this point the data in `buf` should be decrypted in place
				continue;
			case PMALB_ENCRYPTION_TYPE_SYM:
				log_trace("received packet is symmetrically encrypted");
				log_error("stop processing packet here");
				// at this point the data in `buf` should be decrypted in place
				continue;
			default:
				log_warn("encryption method %02x is not supported", packet.flags.encryption_type);
				log_error("stop processing packet here");
				continue;
		}


		dispatch_udp_packet(&packet);
	}

	log_debug("responder was stopped");

	// pthread_exit(&disco_status.thread_exit);

	return 0;
}


int *get_udp_socket() {
	return &udp_s;
}




void *start_udp_server() {
	prepare_udp_socket(&udp_s);
	udp_server_loop(&udp_s);
	return NULL;
}





bool is_multicast_address(struct in6_addr addr) {
	char a[INET6_ADDRSTRLEN];
	inet_ntop(AF_INET6, &addr, a, INET6_ADDRSTRLEN);
	unsigned char *address = (unsigned char*) &addr;

	log_trace("check %s for multicast address: %x == %x", a, addr, 0xff);
	return address[0] == 0xff;
}
