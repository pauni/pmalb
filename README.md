Pmalb - Pass Me A Link Bro
==================
Pass Links, files, mouse-movements and more from your Android to your GNU/Linux machine. GSF **independent**.

Build status:
* Pmalb Desktop ![pipeline status](https://gitlab.com/pauni/pmalb/badges/master/pipeline.svg)
* Pmalb Android ![pipeline status](https://gitlab.com/pauni/pmalb-android/badges/master/pipeline.svg)

Installation - Get the latest builds:
====================
  * [Android](https://pauni.gitlab.io/pmalb-android/latest.apk)
  * [Desktop](https://bit.ly/2m2oa1m)


Documentation
=============

1.  [Header for UDP](#udp_ref)
2.  [Header for TCP](#tcp_ref)
3.  [Pairing](#pairing_ref)
4.  [Discovery](#disco_ref)

### Address, port and type definitions:
__type definitions__

| **Type name**     | **Value** |
| ----------------- | --------- |
| Discovery beacon  | `0`       |
| Pair request      | `1`       |
| (hyper) link      | `2`       |
| mouse input       | `3`       |
| keyboard input    | `4`       |
| media control     | `5`       |

__Adress and port__

| **Attribute**     | **Value**               |
| ----------------- | ----------------------- |
| Multicast address | `ff12:dead:d1cc:fab::1` |
| Port for TCP/UDP  | `4422`                  |


Header for UDP 📡 <a name="udp_ref"></a>
=========

__Header__
```
    0               8              16                            32
    ┌───────────────┬──────────────┬──────────────────────────────┐
  0 | Version       | Flags        | Type                         |
    ├───────────────┴──────────────┴──────────────────────────────┤
  4 |                          Payload                            |
    +-------------------------------------------------------------+
```

__Version__  
Unsigned 8-bit integer. Only increase on backwards incompatible changes.


__Flags__ *LSB → MSB*

| **Bits**  | **Flags**                                             |
| -----     | ------                                                |
| `0-1`     | Encryption type: unsigned 2-bit integer.              |
| `2`       | Multicasted: if packet was multicasted (or unicasted) |
| `3`       | Public Reply: __Not Used__                            |
| `4-7`     | Reserved.  __Not Used__                               |


__Encryption__

When encryption type is **not** `unencrypted`, the upcoming bytes need to be decrypted.  
Keys for decryption are choosen by matching the IP-address against paired-devices list.

__Encryption types__

| **Type name**             | **Value** |   
| -----                     | ------    |
| Unencrypted               | `0`       |
| Asymmetrically encrypted  | `1`       |
| Symmetrically encrypted   | `2`       |   


__Type__ *uint8*

| **Type name**   | **Value** |
| -----           | ------    |
| disco beacon    | `0`       |
| mouse action 🐁 | `2`       |


Header for TCP 🌐 <a name="tcp_ref"></a>
==========

**Header**
```
        0              8              16                             32
    ┌─────────────────────────────────────────────────────────────┐
  0 | payload size                                                |
  4 |                                                             |
    ├──────────────┬──────────────┬───────────────────────────────┤
  8 | Flags        | Version      | Type                          |
    ├──────────────┴──────────────┴───────────────────────────────┤
    |                          Payload                            |
    +-------------------------------------------------------------+
```

Disco beacon 🕺  <a name="disco_ref"></a>
=========

Discovery beacons are sent unencrypted, via UDP and of fixed size.  
The phone sends them as it's ip address changes. The desktop sends them in a regular interval.
Multicasted disco beacons are being answered with a directeded beacon, when they
came from a paired device.

__Multicast address__
`ff12:dead:d1cc:fab::1`

__Structure__
  * `bytes 0-31` public key of the sending device
  * Maybe other data in future eg. signature.


Note: although discovery packets can just as easily be spoofed just as ARP tables,  
that doesn't affect the security of the protocol, as we initially only encrypt data  
with keys, whos fingerprints are encoded in a QR-code


Pairing - create a secured connection 🐧 <a name="pairing_ref"></a>
=========

Pairing data is of the following form:

| **JSON-Key**      | **Value**                 | **Type**   |
| ----------------- | ------------------------- | ---------- |
| *pairing_step*    | step of pairing process   |  int       |
| *device_name*     | human-readable name       |  String    |
| *device_id*       | ID                        |  String    |
| *secret_number*   | secret number from QR-code| int_32     |

Pairing step:  
request = `1`, response = `2`, confirmation = `3`,


**1. Desktop**
  - generate QR-Code containing hexencoded data

    | **Bytes**     | **Value**                              |
    | ------------- | -------------------------------------- |
    | `bytes 0-3`   | randomly generated secret 32bit number |
    | `bytes 4-12`  | first 8 bytes of public key            |


**2. Phone**
  - scan the QR-code
  - send **Pairing-request** (encrypted)

**3. Desktop**
  - verify signature and secret number
  - save the phone to 'paired devices'
  - send pair response to phone 💌

**4. Phone**
  - save the computer to 'paired devices'
