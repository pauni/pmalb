Discovery - discover pmalb-devices in common network 🐢
=======================================================

    👋

Discovery packets are sent every x secs with UDP multicast "ff12:dead:d1cc:fab::1".
They are unencrypted and unsigned and contain:
  *[byte 0] Protocol flags
  *[bytes 01-32] public key of the sending device
  *Maybe other data in future.

Flag Bits (msb to lsb):
  (1): set if packet was unicasted
  (2): set if packet is asymmetrically encrypted for the recipient.
  (3-8): not used


Flag Bits (MSB to LSB):
     [0-3]: 4bit integer for packet type
     [4-7]: packet type specific flags

Packet types:
  [int 0]: Discovery packet
    * Flags:
      (0): set if unicast

  [int 1]: Encrypted paket. Usally unicasted
    * Flags: none


Note: although discovery packets can easily be spoofed, that doesn't affect
the security of the protocol, as we only encrypt data with keys, which fingerprints
are encoded in a QR-code

1
Pairing - create (mutual, secured) connection 🐧
================================================

0) Discover devices

1) Desktop 🖥:
    - generate QR-code, containing hex-encoded:

            * [8 bytes] secret number (randomly generated) 🔢🤫

            * [8 bytes] CRC32 hash of it's publickey 🔑🗣


2) Phone 🤳🖥:
    - scans QR-code, sends assymetrically encrypted and signed pairing-request containing:
        * the secret number 🔢🤫
        * publickey 🗣🔑🗣
        * human-readable device name 🥐
        * fingerprint ☝
        * signature 🔏

3) Desktop :
    - verify signature and secret number ✅.
    - save the phone to 'paired devices' 💾.
    - send back to phone 💌:
        * publickey
        * human-readable device name
        * fingerprint ☝
        * signature 🔏

4) Phone:
    - save the computer to 'paired devices' 💻💾
    - be happy :)🌞
