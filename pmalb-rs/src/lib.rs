pub mod userdata;

use serde;


#[no_mangle]
pub extern fn foobar() -> i64 {
    println!("Hello from Rust");
    0
}


#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
