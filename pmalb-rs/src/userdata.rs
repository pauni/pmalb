use rust_sodium as sodium;
use serde::{Serialize, Deserialize};
use serde_json as json;
use std::path;
use std::fs;
use std::io::{Read, Write};
use sodium::crypto::box_ as cryptobox;



const PREFERENCES_FILE: &str = "./pmalb.json";




#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PreferencesStorage {
	public: String,
	secret: String,
	devices: Vec<PreferencesStorageDevices>,
}



#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct PreferencesStorageDevices {
	name: String,
}



pub struct PairedDevice {
	name: String,
	public_key: Vec<u8>,
	last_seen: u64,
	ip_addr: String,
}


pub struct DeviceManager {
	secret: Vec<u8>,
	public: Vec<u8>,
	devices: Vec<PairedDevice>,
}


impl DeviceManager {
	fn new() -> Self {
		Self {
			secret: vec![0],
			public: vec![0],
			devices: vec![]
		}
	}
}



#[no_mangle]
pub extern fn init_devicemanager() -> *const DeviceManager {
	println!("rust: init devicemanager");
	let pref_file = path::Path::new(PREFERENCES_FILE);
	let prefs: PreferencesStorage;




	if pref_file.exists() {
		let fh = fs::File::open(PREFERENCES_FILE).unwrap();
		prefs = json::from_reader(fh).unwrap();


	}
	else {
		let (pk, sk) = cryptobox::gen_keypair();
	}



	let dvmngr = DeviceManager::new();

	return &dvmngr as *const DeviceManager;
}
