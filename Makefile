CC=gcc
CFLAGS= -fdiagnostics-color=always \
	-g3 -O0 \
	-Wall \
	-lm \
	-DLOG_USE_COLOR \
	-DBUILD_DEBUG \
	-Werror=incompatible-pointer-types \
	-Werror=int-conversion

TARGET=pmalb
LD=ld

OBJECTS=pmalb.o \
		gui.o \
		net.o \
		ipc.o \
		qrcodegen.o \
		protocol.o \
		userdata.o \
		log.o \
		discovery.o


all: $(TARGET) doc

%.o : src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET): $(OBJECTS)
	$(CC) $(CFLAGS) `pkg-config --cflags gtk+-3.0` $(OBJECTS) `pkg-config --libs gtk+-3.0 --libs dbus-1 --libs jansson --libs libsodium` -lm -no-pie -o $(TARGET)

pmalb.o: src/pmalb.c
	$(CC) $(CFLAGS) `pkg-config --cflags dbus-1 ` -c $< -o $@

gui.o: src/gui.c
	$(CC) $(CFLAGS) -c `pkg-config --cflags gtk+-3.0` $< `pkg-config --libs gtk+-3.0 --libs jansson -lm` -o $@

qrcodegen.o: src/qrcodegen.c
	$(CC) $(CFLAGS) -c -fPIC -shared $< -o $@


doc:
	mkdir doc
	doxygen


clean:
	rm -f $(TARGET)
	rm -f $(OBJECTS)
	rm -f /tmp/pmalb_socket
	rm -rf doc/

run: $(TARGET)
	./$(TARGET)
